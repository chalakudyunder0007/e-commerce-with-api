const productTitle=document.getElementById('productTitle')
const mainimage=document.getElementById('mainImage')
const price=document.getElementById('price')
const productDescription=document.getElementById('productDescription')
const subdiv=document.getElementById('listingDiv')
const maindiv=document.getElementById('main')
const sectionhead=document.getElementById('section')



const searchparam=new URLSearchParams(window.location.search);
console.log(searchparam)
let productId
if(searchparam.has('productId')){
   productId=searchparam.has('productId')
}else{
    productId=1
}


fetch('https://fakestoreapi.com/products/'+productId)
.then(res=>res.json())
.then((mainproduct)=>{
    console.log(mainproduct)
    productTitle.innerHTML=mainproduct.title
    mainimage.src=mainproduct.image
    price.innerHTML=mainproduct.price
    productDescription.innerHTML=mainproduct.description
})

// calling api get array of product
fetch('https://fakestoreapi.com/products')
   .then((response)=>(response.json()))
   // we get product list
   .then((productslist)=>{
    //product list to array strat form 0 to 20 
    // useing loop
   for(let i=0;i<productslist.length;i++){
    const product=(productslist[i])//each time product are print of them
    const article=document.createElement('article')// creative aratical tag
   article.classList.add('product')// creative class
   
   const img=document.createElement('img')//creat a image tag
   img.src=product.image// useing for loop and pick one by one by pick
   article.appendChild(img)
   const productdetailsdiv=document.createElement('div')
   productdetailsdiv.classList.add('productdetails')
   article.appendChild(productdetailsdiv)
 // creative link wit h in a tag
 const titlelink=document.createElement('a')
 titlelink.href='/index.html?productid'+product.id




   const h3=document.createElement('h3')
   h3.classList.add('h6')
   h3.innerHTML=product.title

   // a tag  add here

  titlelink.appendChild(h3)
  productdetailsdiv.appendChild(titlelink)
   const ratingdiv=document.createElement('div')
   productdetailsdiv.appendChild(ratingdiv)
   for(let i=0;i<4;i++){
    const ratingfilledspan=document.createElement('span')
    ratingfilledspan.innerHTML='&#9733;'
    ratingdiv.appendChild(ratingfilledspan)
   }
   const ratingemptyspan=document.createElement('span')
   ratingemptyspan.innerHTML='&#9734;'
   ratingdiv.appendChild(ratingemptyspan)

   const priceandbuttondiv=document.createElement('div')
   priceandbuttondiv.classList.add('priceandbutton')

   productdetailsdiv.appendChild(priceandbuttondiv)
    const pricespan=document.createElement('span')
    pricespan.classList.add('p')
    pricespan.innerHTML=product.price
    priceandbuttondiv.appendChild(pricespan)

    const buttonspan=document.createElement('button')
    buttonspan.classList.add('button')
    buttonspan.classList.add('buttonprimary')
    buttonspan.innerHTML='add to car'
    priceandbuttondiv.appendChild(buttonspan)
    subdiv.appendChild(article)
    maindiv.appendChild(subdiv)
    sectionhead.appendChild(maindiv)
    

   console.log(article)
   }
   })
   .catch(error=>console.log(error))